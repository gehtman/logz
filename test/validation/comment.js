var Joi = require('joi');

module.exports = {
  body: {
    email: Joi.string().email().required(),
    text: Joi.string().alphanum().min(1).max(1000).required(),
  }
};
