
// comments/comments.controller.js

(function() {
    'use strict';

    angular
        .module('logzExcerciseApp')
        .controller('CommentsController', CommentsController);

    function CommentsController(dataService, logger) {
      var vm = this;

      vm.comments = [];
      vm.newComment = '';
      vm.filterText = '';
      vm.email = '';
      vm.submitInProggress = false;
      vm.errorMessage = '';
      vm.validateComment = validateComment;
      vm.submitComment = submitComment;

      activate();

      function activate() {
        getComments().then(function() {
          logger.info('Activated Comments View');
        });
      }

      function getComments() {
        return dataService.getComments().then(function(data) {
          vm.comments = data;
          return vm.comments;
        });
      }

      function submitComment(){
        vm.submitInProggress = true;

        dataService.submitComment({ text: vm.newComment, email: vm.email, created: new Date() }).then(function(data) {
          logger.info('Comment submitted');
          vm.comments.unshift(data);
          vm.submitInProggress = false;
          vm.filterText = '';
          vm.newComment = '';
          return vm.comments;
        }).catch(function(error){
          vm.submitInProggress = false;
        });
      }

      function validateComment() {
        //no client side validation, the exercise requires server side validation.
        return true;
      }
    }
})();
