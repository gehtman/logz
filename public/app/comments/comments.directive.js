
// comments/comments.directive.js

(function() {
    'use strict';

    angular
        .module('logzExcerciseApp')
        .directive('comments', comments);

    function comments() {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'app/comments/comments.html',
            controller: 'CommentsController',
            controllerAs: 'vm',
            bindToController: true // because the scope is isolated
        };
    }
})();
