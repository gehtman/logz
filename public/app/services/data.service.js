
// services/data.service.js

(function() {
    'use strict';

    angular
        .module('logzExcerciseApp')
        .factory('dataService', dataService);

    dataService.$inject = ['$http', 'logger', '$q'];

    function dataService($http, logger, $q) {
        return {
            getComments: getComments,
            submitComment: submitComment
        };

        function submitComment(comment) {
            return $http.post('/api/comments/new', comment)
                .then(submitCommentComplete)
                .catch(submitCommentFailed);

            function submitCommentComplete(response) {
                logger.info('XHR Completed for submitComment');
                return response.data;
            }

            function submitCommentFailed(e) {
              logError('submitComment', e.data);
              return $q.reject(e);
            }
        }

        function getComments() {
            return $http.get('/api/comments')
                .then(getCommentsComplete)
                .catch(getCommentsFailed);

            function getCommentsComplete(response) {
                logger.info('XHR Completed for getComments');
                return response.data;
            }

            function getCommentsFailed(e) {
              logError('getComments', e.data);
              return $q.reject(e);
            }
        }

        function logError(name, message){
          logger.error('Data operation failed for ' + name + ': ' + message);
        }
    }
})();
