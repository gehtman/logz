
// services/logger.service.js

(function() {
    'use strict';

    angular
        .module('logzExcerciseApp')
        .factory('logger', logger);

    function logger() {
        return {
            error: log,
            warning: log,
            info: log
        };

        function log(msg){
          console.log(msg)
        }
    }
})();
