var inMemoryRepo = require('./inMemoryRepo');

class CommentsRepo {
  constructor(){
    this.repo = new inMemoryRepo.InMemoryRepo();
  }

  findAll(){
    return this.repo.findAll();
  }

  insert(comment){
    return this.repo.insert(comment);
  }
}

module.exports = {
  CommentsRepo: CommentsRepo
};
