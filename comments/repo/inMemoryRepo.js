var commentsGenerator = require('../commentsGenerator');

class InMemoryRepo {
  constructor(height, width) {
    this.comments = commentsGenerator.generateComments();
  }

  findAll(){
    return this.comments;
  }

  insert(comment){
    this.comments.push(comment);
    return comment;
  }
}

module.exports = {
  InMemoryRepo: InMemoryRepo
};
