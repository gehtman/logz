var gravatar = require('gravatar');

function addGravatarAndDate(avatarKey, dateKey){
  return function(req, res, next) {
    let email = req.body.email,
        text = req.body.text;
    req.body = {
      email,
      text,
      [avatarKey]: gravatar.url(req.body.email, { protocol: 'https', s: '100', d: 'mm' }),
      [dateKey]: new Date()
    }
    next();
  }
}

module.exports = {
  addGravatarAndDate: addGravatarAndDate
};
