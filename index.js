var path = require('path');
var bodyParser = require('body-parser');

var express = require('express')
var app = express()

var CommentsRepo = require('./comments/repo/commentsRepo').CommentsRepo;
var repo = new CommentsRepo();

var validate = require('express-validation');
var commentValidation = require('./test/validation/comment.js');
var addGravatarAndDate = require('./comments/addGravatarAndDate').addGravatarAndDate;

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(function(err, req, res, next){
  res.status(400).json(err);
});
app.get('/', function(req, res) {
  res.sendFile(path.resolve('./index.html'));
});

app.get('/api/comments/', function (req, res) {
  res.json(repo.findAll());
});

app.post('/api/comments/new', validate(commentValidation), addGravatarAndDate('avatar', 'created'), function(req, res) {
  res.json(repo.insert(req.body));
});

app.listen(3000, function () {
  console.log('App listening on port 3000!');
});
